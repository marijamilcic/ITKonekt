package rs.saga.itconnect.test.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import rs.saga.itconnect.config.ITConnectConfig;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.exception.TopicNotExistException;
import rs.saga.itconnect.service.ITopicService;
import rs.saga.itconnect.test.builder.TopicBuilder;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ITConnectConfig.class)
public class TopicServiceTest {

    @Autowired
    private ITopicService topicService;

    private Topic topic;

    @Before
    public void loadData(){
        topic = new TopicBuilder().topic("title", "description", "category");
        topicService.saveOrUpdateTopic(topic);
    }

    @Test(expected = TopicNotExistException.class)
    public void testGetByIdException(){
        topicService.getTopicByID(0);
    }

    @Test
    public void testGetById(){
        Topic dbTopic = topicService.getTopicByID(topic.getId());
        assertEquals(dbTopic.getTitle(), topic.getTitle());
    }
    @Test
    public void testUpdate(){
        topic.setTitle("title1");
        topicService.saveOrUpdateTopic(topic);
        Topic dbTopic = topicService.getTopicByID(topic.getId());
        assertEquals(dbTopic.getTitle(), topic.getTitle());
    }

    @Test
    public void testGetTopics(){
        Iterable<Topic> dbTopics = topicService.getTopics();
        List<Topic> list = (List<Topic>) dbTopics;
        assertEquals(list.size(), 1);
    }
    @Test(expected = TopicNotExistException.class)
    public void testDeleteByID(){
        topicService.deleteTopic(topic.getId());
        topicService.getTopicByID(topic.getId());
    }

    @Test
    public void testDeleteTopics(){
        topicService.deleteAllTopics();
        Iterable<Topic> dbTopics = topicService.getTopics();
        List<Topic> list = (List<Topic>) dbTopics;
        assertEquals(list.size(), 0);
    }
    @After
    public void tearDownData(){
        topicService.deleteAllTopics();
    }

}
