package rs.saga.itconnect.test.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationExcludeFilter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import rs.saga.itconnect.entity.Speaker;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.rest.SpeakerREST;
import rs.saga.itconnect.service.ITSpeakerService;
import rs.saga.itconnect.test.builder.SpeakerBuilder;
import rs.saga.itconnect.test.builder.TopicBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(SpeakerREST.class)
@ContextConfiguration
public class SpeakerRestMockOnlyWebTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private ITSpeakerService speakerService;

    @Test
    public void testGetSpeaker() throws Exception {

        when(speakerService.getSpeakerByID(1)).thenReturn(new SpeakerBuilder().speaker("name", "surname", "organization",
                new TopicBuilder().topic("t", "d", "c")));

        this.mockMvc.perform(get("/speaker/id/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("name"))).andReturn();

        ResultActions resultActions= this.mockMvc.perform(get("/speaker/id/1"));
//        https://github.com/json-path/JsonPath
        resultActions.andExpect(jsonPath("$.name").value("name"));

    }

    @Test
    public void testInsertSpeaker() throws Exception {
        Topic topic3 = new TopicBuilder().topic("title3", "description3", "category3");
        Speaker speaker3 = new SpeakerBuilder().speaker("name3", "surname3", "organization3", topic3);
        String jsonInString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(speaker3);

        this.mockMvc.perform(post("/speaker/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("name3")));

        MvcResult mvcResult = this.mockMvc.perform(post("/speaker/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(post("/speaker/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString));
        resultActions.andExpect(jsonPath("$.name").value("name3"));

    }


    @Configuration
    @EnableAutoConfiguration
    @ComponentScan(basePackages = "rs.saga.itconnect", excludeFilters = {
            @ComponentScan.Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
            @ComponentScan.Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
    static class TestConfig {

    }
}
