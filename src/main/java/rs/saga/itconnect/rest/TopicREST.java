package rs.saga.itconnect.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.service.ITopicService;

import java.util.Collection;

/**
 * Created by mmilcic on 4/5/2018.
 * REST
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/topic")
public class TopicREST {


    private ITopicService topicService;

    @Autowired
    public TopicREST(ITopicService topicService){
        this.topicService = topicService;
    }

    @GetMapping(value = "/id/{id}")
    @ResponseBody
    public Topic getTopic(@PathVariable(value = "id") Integer id) {
        Topic topic = topicService.getTopicByID(id);
        return topic;
    }

    @PostMapping("/new")
    public ResponseEntity<?> insert(@RequestBody Topic topic){
        topicService.saveOrUpdateTopic(topic);
        return ResponseEntity.ok(topic);
    }

    @GetMapping(value = "/all")
    @ResponseBody
    public Collection<Topic> getAll(){
        return (Collection<Topic>) topicService.getTopics();
    }


    @PutMapping(value = "/update")
    public ResponseEntity update(@RequestBody Topic topic){
        topicService.saveOrUpdateTopic(topic);
        return ResponseEntity.ok(topic);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity delete(@PathVariable Integer id){
        topicService.deleteTopic(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/delete/all")
    public ResponseEntity deleteAll(){
        topicService.deleteAllTopics();
        return ResponseEntity.ok().build();
    }



}
