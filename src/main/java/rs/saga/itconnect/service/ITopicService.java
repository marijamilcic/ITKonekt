package rs.saga.itconnect.service;

import rs.saga.itconnect.entity.Topic;

public interface ITopicService {
    Topic getTopicByID(Integer id);

    void saveOrUpdateTopic(Topic topic);

    Iterable<Topic> getTopics();

    void deleteTopic(Integer id);

    void deleteAllTopics();


}
