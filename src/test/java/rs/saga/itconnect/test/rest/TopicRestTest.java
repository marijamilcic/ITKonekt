package rs.saga.itconnect.test.rest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import rs.saga.itconnect.config.ITConnectConfig;
import rs.saga.itconnect.dao.TopicDAO;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.test.builder.TopicBuilder;

import java.util.Collection;

import static org.junit.Assert.*;

@SuppressWarnings("Duplicates")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ITConnectConfig.class, webEnvironment  = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TopicRestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TopicDAO topicDAO;

    private Topic topic1;

    @Before
    public void loadData(){
        topic1 = new Topic();
        topic1.setTitle("title");
        topic1.setDescription("description");
        topic1.setCategory("categoty");
        topicDAO.save(topic1);

        Topic topic2 = new Topic();
        topic2.setTitle("title");
        topic2.setDescription("description");
        topic2.setCategory("categoty");
        topicDAO.save(topic2);

    }

    @Test
    public void testGetTopicNoDataExist() {

        ResponseEntity<Topic> response = this.restTemplate.getForEntity("http://localhost:" + port + "/topic/id/0", Topic.class);
        assertEquals("status not valid", HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        Topic topic = response.getBody();
        assertNull(topic.getTitle());
    }

    @Test
    public void testGetTopicDataExist() {

        ResponseEntity<Topic> responseTopic = this.restTemplate.getForEntity("http://localhost:" + port + "/topic/id/" + topic1.getId(), Topic.class);
        assertEquals("status not valid", HttpStatus.OK, responseTopic.getStatusCode());
        Topic topic = responseTopic.getBody();
        assertNotNull(topic.getTitle());

        ResponseEntity<Collection> responseCollection = this.restTemplate.getForEntity("http://localhost:" + port + "/topic/all", Collection.class);
        assertEquals("status not valid", HttpStatus.OK, responseCollection.getStatusCode());
        assertEquals("size not valid", 2, responseCollection.getBody().size());
    }

    @Test
    public void testInsertTopic() {
        Topic topic3 = new TopicBuilder().topic("title3", "description3", "category");
        ResponseEntity<Topic> response = this.restTemplate.postForEntity("http://localhost:" + port + "/topic/new", topic3, Topic.class);
        assertEquals("status not valid", HttpStatus.OK, response.getStatusCode());
        Topic topic = response.getBody();
        assertEquals("titles not equals", topic3.getTitle(), topic.getTitle());
    }

    @Test
    public void testUpdateTopic() {
        topic1.setTitle("newTitle");
        this.restTemplate.put("http://localhost:" + port + "/topic/update", topic1);

        ResponseEntity<Topic> responseTopic = this.restTemplate.getForEntity("http://localhost:" + port + "/topic/id/" + topic1.getId(), Topic.class);
        assertEquals("status not valid", HttpStatus.OK, responseTopic.getStatusCode());
        Topic topic = responseTopic.getBody();
        assertNotNull(topic.getTitle());
    }

    @Test
    public void testDeleteTopic() {
        this.restTemplate.delete("http://localhost:" + port + "/topic/delete/"+topic1.getId());
        ResponseEntity<Topic> response = this.restTemplate.getForEntity("http://localhost:" + port + "/topic/id/"+topic1.getId(), Topic.class);
        assertEquals("status not valid", HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        Topic topic = response.getBody();
        assertNull(topic.getTitle());
    }

    @After
    public void tearDownData(){
        topicDAO.deleteAll();
    }

}
