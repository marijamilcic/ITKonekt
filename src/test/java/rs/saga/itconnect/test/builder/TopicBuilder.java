package rs.saga.itconnect.test.builder;

import rs.saga.itconnect.entity.Topic;

public class TopicBuilder {

    private Topic model;

    public TopicBuilder() {
        model = new Topic();
    }

    private TopicBuilder id(Integer id) {
        model.setId(id);
        return this;
    }
    private TopicBuilder title(String title) {
        model.setTitle(title);
        return this;
    }

    private TopicBuilder description(String description) {
        model.setDescription(description);
        return this;
    }

    private TopicBuilder category(String category) {
        model.setCategory(category);
        return this;
    }

    private Topic build() {
        return model;
    }


    public Topic topic(String title, String description, String category) {
        return new TopicBuilder().title(title).description(description).category(category).build();
    }

    public Topic topicWithID(Integer id, String title, String description, String category) {
        return new TopicBuilder().id(id).title(title).description(description).category(category).build();
    }

}
