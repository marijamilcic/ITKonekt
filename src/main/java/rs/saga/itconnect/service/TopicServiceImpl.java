package rs.saga.itconnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.saga.itconnect.dao.TopicDAO;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.exception.TopicNotExistException;

import java.util.Optional;

/**
 * Created by mmilcic on 4/5/2018.
 * Service class
 */
@Service
public class TopicServiceImpl implements ITopicService {

    private TopicDAO topicDAO;

    @Autowired
    public TopicServiceImpl(TopicDAO topicDAO){
        this.topicDAO = topicDAO;
    }

    public void saveOrUpdateTopic(Topic topic){
        topicDAO.save(topic);
    }

    public Topic getTopicByID(Integer id){
        Optional<Topic> topic = topicDAO.findById(id);
        if(topic.isPresent()){
            return topic.get();
        }else{
            throw new TopicNotExistException("Topic does not exist");
        }
    }

    public Iterable<Topic> getTopics(){
        return topicDAO.findAll();
    }


    public void deleteTopic(Integer id){
        topicDAO.findById(id).ifPresent(topic -> topicDAO.delete(topic));
    }

    public void deleteAllTopics(){
        topicDAO.deleteAll();
    }
}
