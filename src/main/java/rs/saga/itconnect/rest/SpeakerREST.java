package rs.saga.itconnect.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.saga.itconnect.entity.Speaker;
import rs.saga.itconnect.service.ITSpeakerService;

import java.util.Collection;

/**
 * Created by mmilcic on 4/5/2018.
 * REST
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/speaker")
public class SpeakerREST {

    private ITSpeakerService speakerService;

    @Autowired
    public SpeakerREST(ITSpeakerService speakerService){
        this.speakerService = speakerService;
    }

    @GetMapping(value = "/id/{id}")
    @ResponseBody
    Speaker getSpeaker(@PathVariable Integer id) {
        return speakerService.getSpeakerByID(id);
    }

    @PostMapping(value = "new")
    ResponseEntity<?> insert(@RequestBody Speaker speaker){
        speakerService.saveOrUpdateSpeaker(speaker);
        return ResponseEntity.ok(speaker);
    }

    @GetMapping(value = "/all")
    @ResponseBody
    Collection<Speaker> getAll(){
        return (Collection<Speaker>) speakerService.getSpeakers();
    }


    @PutMapping(value = "/update")
    public ResponseEntity update(@RequestBody Speaker speaker){
        speakerService.saveOrUpdateSpeaker(speaker);
        return ResponseEntity.ok(speaker);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity delete(@PathVariable Integer id){
        speakerService.deleteSpeaker(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/delete/all")
    public ResponseEntity deleteAll(){
        speakerService.deleteAllSpeakers();
        return ResponseEntity.ok().build();
    }

}
