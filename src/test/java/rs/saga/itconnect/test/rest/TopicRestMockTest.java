package rs.saga.itconnect.test.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import rs.saga.itconnect.config.ITConnectConfig;
import rs.saga.itconnect.dao.TopicDAO;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.test.builder.TopicBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SuppressWarnings("Duplicates")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ITConnectConfig.class)
@AutoConfigureMockMvc
public class TopicRestMockTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TopicDAO topicDAO;

    @Autowired
    ObjectMapper objectMapper;

    private Topic topic1;

    @Before
    public void loadData(){
        topic1 = new Topic();
        topic1.setTitle("title");
        topic1.setDescription("description");
        topic1.setCategory("categoty");
        topicDAO.save(topic1);

        Topic topic2 = new Topic();
        topic2.setTitle("title");
        topic2.setDescription("description");
        topic2.setCategory("categoty");
        topicDAO.save(topic2);
    }

    @Test
    public void testGetTopic() throws Exception {
        this.mockMvc.perform(get("/topic/id/" + topic1.getId())).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("title")));

        MvcResult mvcResult = this.mockMvc.perform(get("/topic/id/" + + topic1.getId())).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(get("/topic/id/" + + topic1.getId()));
        resultActions.andExpect(jsonPath("$.title").value("title"));

    }

    @Test
    public void testGetTopics() throws Exception {
        this.mockMvc.perform(get("/topic/all")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("title")));

        MvcResult mvcResult = this.mockMvc.perform(get("/topic/all")).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(get("/topic/all"));
        resultActions.andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    public void testDeleteTopic() throws Exception {
        this.mockMvc.perform(delete("/topic/delete/" + + topic1.getId())).andDo(print()).andExpect(status().isOk());

        MvcResult mvcResult = this.mockMvc.perform(delete("/topic/delete/" + + topic1.getId())).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(delete("/topic/delete/" + + topic1.getId()));
        resultActions.andExpect(body -> {});

    }

    @Test
    public void testUpdateTopic() throws Exception {
        topic1.setTitle("title3");
        String jsonInString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(topic1);
        this.mockMvc.perform(put("/topic/update").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andExpect(status().isOk());

        MvcResult mvcResult = this.mockMvc.perform(put("/topic/update").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(put("/topic/update").contentType(MediaType.APPLICATION_JSON).content(jsonInString));
        resultActions.andExpect(jsonPath("$.title").value("title3"));

    }

    @Test
    public void testInsertTopic() throws Exception {
        Topic topic3 = new TopicBuilder().topic("title3", "description3", "category3");
        String jsonInString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(topic3);
        this.mockMvc.perform(post("/topic/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andExpect(status().isOk());

        MvcResult mvcResult = this.mockMvc.perform(post("/topic/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(post("/topic/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString));
        resultActions.andExpect(jsonPath("$.title").value("title3"));

    }

    @After
    public void tearDownData(){
        topicDAO.deleteAll();
    }


}
